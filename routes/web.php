<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  ["uses"=>"ProductsController@index", "as"=> "allProducts"]);



//show all products
Route::get('products', ["uses"=>"ProductsController@index", "as"=> "allProducts"]);


//add to cart
Route::get('product/addToCart/{id}',['uses'=>'ProductsController@addProductToCart','as'=>'AddToCartProduct']);


//show cart items
Route::get('cart', ["uses"=>"ProductsController@showCart", "as"=> "cartproducts"]);


//delete item from cart
Route::get('product/deleteItemFromCart/{id}',['uses'=>'ProductsController@deleteItemFromCart','as'=>'DeleteItemFromCart']);





//increase single product in cart
Route::get('product/increaseSingleProduct/{id}',['uses'=>'ProductsController@increaseSingleProduct','as'=>'IncreaseSingleProduct']);


//decrease single product in cart
Route::get('product/decreaseSingleProduct/{id}',['uses'=>'ProductsController@decreaseSingleProduct','as'=>'DecreaseSingleProduct']);



Route::get('product/checkoutProducts/',['uses'=>'ProductsController@checkoutProducts','as'=>'checkoutProducts']);




//checkout page
Route::get('product/checkoutProducts/',['uses'=>'ProductsController@checkoutProducts','as'=>'checkoutProducts']);



//process checkout page
Route::post('product/createNewOrder/',['uses'=>'ProductsController@createNewOrder','as'=>'createNewOrder']);


//create an order
Route::get('product/createOrder/',['uses'=>'ProductsController@createOrder','as'=>'createOrder']);





//payment page
Route::get('payment/paymentpage', ["uses"=> "Payment\PaymentsController@showPaymentPage", 'as'=> 'showPaymentPage']);


//process payment & receipt page
Route::get('payment/paymentreceipt/{paymentID}/{payerID}', ["uses"=> "Payment\PaymentsController@showPaymentReceipt", 'as'=> 'showPaymentReceipt']);








//User Authentication
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



//admin routes start

Route::get('/admin', 'AdminController@login')->name('admin');


Route::get('/admindashboard', 'AdminController@admindashboard');


Route::get('/addProduct', 'AdminController@addProduct');


Route::post('/addProduct', 'AdminController@createProduct');



Route::get('/viewProducts', 'AdminController@viewProducts');


Route::get('/category_product/{id}', 'ProductsController@category_product');



Route::get('/viewOrders', 'AdminController@viewOrders');

Route::get('/viewUsers', 'AdminController@viewUsers');

Route::get('/orders', 'AdminController@orders');

Route::get('/order_confirm/{id}', 'AdminController@order_confirm');

//admin routes end



Route::get('/logout', 'HomeController@logout');




//storage
Route::get('/session',function(\Illuminate\Http\Request $request){

 dd($request->session()->all());
});








