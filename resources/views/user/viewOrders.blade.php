
@extends('layouts.index')



@section('center')
<div class="container">

    <h2>Order List</h2>

    <table class="table">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Price</th>
          <th>Address</th>
          <th>phone</th>
          <th>zip</th>
          <th>email</th>
        </tr>
      </thead>
      <tbody>

          @foreach ($orders as $item)
          <tr>
            <td>{{ $item->first_name}}</td>
            <td>{{ $item->last_name}}</td>
            <td>{{ $item->price}}</td>
            <td>{{ $item->address}}</td>
            <td>{{ $item->phone}}</td>
            <td>{{ $item->zip}}</td>
            <td>{{ $item->email}}</td>
          </tr>
          @endforeach


      </tbody>
    </table>
  </div>


    @endsection


