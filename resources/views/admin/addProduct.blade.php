
@extends('admin.index')



@section('center')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
<form class="form-horizontal" action="{{ url('addProduct') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <fieldset>

    <!-- Form Name -->
    <legend>PRODUCTS</legend>



    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="product_name">PRODUCT NAME</label>
      <div class="col-md-4">
      <input id="product_name" name="name" placeholder="PRODUCT NAME" class="form-control input-md" required="" type="text">

      </div>
    </div>
  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-4 control-label" for="product_name">PRODUCT PRICE</label>
    <div class="col-md-4">
    <input id="product_name" name="price" placeholder="PRODUCT NAME" class="form-control input-md" required="" type="text">

    </div>
  </div>

  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-4 control-label" for="product_name">PRODUCT CATEGORY</label>
    <div class="col-md-4">
        <select class="form-control input-md"  name="category">

            <option value="T-Shirt">T-Shirt</option>
            <option value="Polo-shirt">Polo-shirt</option>

        </select>
    </div>
  </div>

    <!-- Textarea -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="product_description">PRODUCT DESCRIPTION</label>
      <div class="col-md-4">
        <textarea class="form-control" id="product_description" name="description"></textarea>
      </div>
    </div>


     <!-- File Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="filebutton">image</label>
      <div class="col-md-4">
        <input  name="image" type="file" id="imageInput">
      </div>
    </div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="singlebutton"></label>
      <div class="col-md-4">
        <button id="singlebutton" name="singlebutton" class="btn btn-primary">Create</button>
      </div>
      </div>

    </fieldset>
    </form>


    @endsection


