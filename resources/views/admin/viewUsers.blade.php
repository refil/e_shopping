
@extends('admin.index')



@section('center')
<div class="container">

    <h2>Users List</h2>

    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Created at</th>


        </tr>
      </thead>
      <tbody>

          @foreach ($users as $item)
          <tr>
            <td>{{ $item->name}}</td>
            <td>{{ $item->email}}</td>
            <td>{{ $item->created_at}}</td>
          </tr>
          @endforeach


      </tbody>
    </table>
  </div>


    @endsection


