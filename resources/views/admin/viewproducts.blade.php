
@extends('admin.index')



@section('center')
<div class="container">

    <h2>Product List</h2>

    <table class="table">
      <thead>
        <tr>
          <th>Product Name</th>
          <th>Image</th>
          <th>Price</th>
          <th>Description</th>
          <th>Category</th>

        </tr>
      </thead>
      <tbody>

          @foreach ($products as $item)
          <tr>
            <td>{{ $item->name}}</td>
          <td><img width="60" src="{{ asset('images/'.$item->image)}}"></td>
            <td>{{ $item->price}}</td>
            <td>{{ $item->description}}</td>
            <td>{{ $item->category}}</td>
          </tr>
          @endforeach


      </tbody>
    </table>
  </div>


    @endsection


