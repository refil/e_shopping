
@extends('layouts.index')



@section('center')

</header><!--/header-->



<section>
    <div class="container">
        <div class="row">


@include('alert')

            <div class="col-sm-12 padding-right">
                <div class="features_items"><!--items-->
                    <h2 class="title text-center">Items</h2>
                </div><!--items-->






                <div class="category-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li >
                                <a href="{{ url('/') }}" >All Products</a></li>

                            <li>
                            <li >
                                <a href="{{ url('/category_product/T-Shirt') }}" >T-Shirt</a>
                            </li>
                            <li>
                                <a href="{{ url('/category_product/Polo-shirt') }}" >Polo-shirt</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tshirt" >


                            @foreach ($products as $product)

                            <div class="col-sm-3">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img width="150" src="{{asset('images/'.$product->image)}}" alt="" />
                                            <h2>AED {{$product->price}}</h2>
                                            <p>{{$product->name}}</p>
                                            <a href=" {{ route('AddToCartProduct',$product->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>


                        <div class="tab-pane fade" id="poloshirt" >

                        </div>
                    </div>
                </div><!--/category-tab-->


            </div>
        </div>
    </div>
</section>
@endsection
