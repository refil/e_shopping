<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Cart;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

use App\Mail\OrderCreatedEmail;
use Illuminate\Support\Facades\Mail;


class ProductsController extends Controller
{

    public function index(){

        $products=Product::all();

        return view("allproducts",['products'=>$products]);

    }

    public static function category_product($id)
    {
        $products=Product::where('category',$id)->get();
        return view("allproducts",['products'=>$products]);
    }


    public function addProductToCart(Request $request,$id){

        //        $request->session()->forget("cart");
        //        $request->session()->flush();

                $prevCart = $request->session()->get('cart');
                $cart = new Cart($prevCart);

                $product = Product::find($id);
                $cart->addItem($id,$product);
                $request->session()->put('cart', $cart);

                //dump($cart);

                return redirect()->route("allProducts");


            }



            public function showCart(){

                $cart = Session::get('cart');

                //cart is not empty
                if($cart){
                    return view('cartproducts',['cartItems'=> $cart]);
                 //cart is empty
                }else{
                    return redirect()->route("allProducts");
                }

            }




            public function deleteItemFromCart(Request $request,$id){

                $cart = $request->session()->get("cart");

                if(array_key_exists($id,$cart->items)){
                    unset($cart->items[$id]);

                }

                $prevCart = $request->session()->get("cart");
                $updatedCart = new Cart($prevCart);
                $updatedCart->updatePriceAndQunatity();

                $request->session()->put("cart",$updatedCart);

                return redirect()->route('cartproducts');



            }




             public function increaseSingleProduct(Request $request,$id){


                $prevCart = $request->session()->get('cart');
                $cart = new Cart($prevCart);

                $product = Product::find($id);
                $cart->addItem($id,$product);
                $request->session()->put('cart', $cart);

                //dump($cart);

                return redirect()->route("cartproducts");


            }








               public function decreaseSingleProduct(Request $request,$id){



                $prevCart = $request->session()->get('cart');
                $cart = new Cart($prevCart);

                if( $cart->items[$id]['quantity'] > 1){
                          $product = Product::find($id);
                          $cart->items[$id]['quantity'] = $cart->items[$id]['quantity']-1;
                          $cart->items[$id]['totalSinglePrice'] = $cart->items[$id]['quantity'] *  $product['price'];
                          $cart->updatePriceAndQunatity();

                          $request->session()->put('cart', $cart);

                  }



                return redirect()->route("cartproducts");


            }

            public function checkoutProducts()
            {

                return view('checkoutproducts');


            }




   public function createNewOrder(Request $request){

    $cart = Session::get('cart');

    $first_name = $request->input('first_name');
    $address = $request->input('address');
    $last_name = $request->input('last_name');
    $zip = $request->input('zip');
    $phone = $request->input('phone');
    $email = $request->input('email');




 //check if user is logged in or not
    $isUserLoggedIn = Auth::check();

   if($isUserLoggedIn){
       //get user id
      $user_id = Auth::id();  //OR $user_id = Auth:user()->id;

   }else{
       //user is guest (not logged in OR Does not have account)
     $user_id = 0;

   }




     //cart is not empty
     if($cart) {
     // dump($cart);
         $date = date('Y-m-d H:i:s');
         $newOrderArray = array("user_id" => $user_id, "status"=>"pending","date"=>$date,"del_date"=>$date,"price"=>$cart->totalPrice,
         "first_name"=>$first_name, "address"=> $address, 'last_name'=>$last_name, 'zip'=>$zip,'email'=>$email,'phone'=>$phone);

         $created_order = DB::table("orders")->insert($newOrderArray);
         $order_id = DB::getPdo()->lastInsertId();;


         foreach ($cart->items as $cart_item){
             $item_id = $cart_item['data']['id'];
             $item_name = $cart_item['data']['name'];
             $item_price = $cart_item['data']['price'];
             $newItemsInCurrentOrder = array("item_id"=>$item_id,"order_id"=>$order_id,"item_name"=>$item_name,"item_price"=>$item_price);
             $created_order_items = DB::table("order_items")->insert($newItemsInCurrentOrder);
         }


         //send the email

         //delete cart
         Session::forget("cart");




         $payment_info =  $newOrderArray;
         $payment_info['order_id'] = $order_id;
         $request->session()->put('payment_info',$payment_info);

         $data = array('email'=>$email);

         Mail::send('emails.orderCreatedEmail', $data, function($message) use ($data)
         {
             $message->to($data['email'])
             ->subject('Order Confirmation');
         });


         //send order email to admin
         Mail::send('emails.orderCreatedEmail', $data, function($message) use ($data)
         {
             $message->to('ashwinbalaji176@gmail.com')
             ->subject('Order Confirmation');
         });

       //print_r($newOrderArray);
       //delete cart
      Session::forget("cart");
    //   Session::flush();
      return redirect()->route("allProducts")->withsuccess("Order has been Placed Successfully, Thanks For Choosing Us");
     }else{

       return redirect()->route("allProducts");


     }



}




 public function createOrder(){
     $cart = Session::get('cart');

     //cart is not empty
     if($cart) {
     // dump($cart);
         $date = date('Y-m-d H:i:s');
         $newOrderArray = array("status"=>"on_hold","date"=>$date,"del_date"=>$date,"price"=>$cart->totalPrice);
         $created_order = DB::table("orders")->insert($newOrderArray);
         $order_id = DB::getPdo()->lastInsertId();;


         foreach ($cart->items as $cart_item){
             $item_id = $cart_item['data']['id'];
             $item_name = $cart_item['data']['name'];
             $item_price = $cart_item['data']['price'];
             $newItemsInCurrentOrder = array("item_id"=>$item_id,"order_id"=>$order_id,"item_name"=>$item_name,"item_price"=>$item_price);
             $created_order_items = DB::table("order_items")->insert($newItemsInCurrentOrder);
         }

         //delete cart
         Session::forget("cart");
         Session::flush();
         return redirect()->route("allProducts")->withsuccess("Thanks For Choosing Us");

     }else{

         return redirect()->route("allProducts");

     }


 }







   private function sendMail(){

       $user = Auth::user();
       $cart = Session::get('cart');

       if($cart != null && $user != null ){
          Mail::to($user)->send(new OrderCreatedEmail($cart));
       }



 }






}






































