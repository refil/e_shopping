<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public static function login()
    {
        return view('admin/login');
    }

    //admin dashboard
    public static function admindashboard()
    {
        return view('admin/adminpanel');
    }

     //add product
    public static function addProduct()
    {
        return view('admin/addProduct');
    }

    public static function createProduct(Request $request)
    {
        $data=$request->all();
      ///  echo json_encode($data);

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);

            $sql=new Product();
            $sql->name=$data['name'];
            $sql->image=$name;
            $sql->description=$data['description'];
            $sql->price=$data['price'];
            $sql->category=$data['category'];
            $sql->save();

           // echo "success";

            return back()->with('success','Image Upload successfully');
        }

    }

    public static function viewProducts()
    {
        $products=Product::all();
        return view('admin/viewproducts',['products'=>$products]);

    }


    public static function viewOrders()
    {
        $orders=Order::all();
        return view('admin/viewOrders',['orders'=>$orders]);

    }

    public static function orders()
    {
        // echo json_encode(auth()->user());
        // exit;
        $orders=Order::where('user_id',auth()->user()->id)->get();
        return view('user/viewOrders',['orders'=>$orders]);

    }

    public static function viewUsers()
    {
        $users=User::where('type','normal')->get();
        return view('admin/viewUsers',['users'=>$users]);

    }


    public static function order_confirm($id)
    {
        Order::where('order_id',$id)
       ->update([
           'status' => 'confirm'
        ]);

        return redirect('viewOrders');


    }






}
