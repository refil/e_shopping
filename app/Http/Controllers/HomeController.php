<?php

namespace App\Http\Controllers;
use Auth;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
          if(auth()->user()->type=='admin')
          {
            return redirect('/admindashboard');
          }
          else
          {
            return view('home');
          }
        }
        else
        {
            return view('home');
        }

    }

    public function logout()
    {
        Auth::logout();
        return view('home');

    }
}
